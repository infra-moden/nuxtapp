export const state = () => {
  return {
    strapiurl: process.env.STRAPI_URL,
    nodeurl: process.env.NODEAPP_URL,
    identifier: process.env.IDENTIFIER,
    katalaluan: process.env.KATALALUAN,
    jwttoken: null,
  }
}
  
export const mutations = {
  setToken (state, jwttoken) {
    state.jwttoken = jwttoken
  },
}